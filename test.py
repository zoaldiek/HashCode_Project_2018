# -*- coding: utf-8 -*-
"""

"""

from instance import *
from solution import *

# files = ["a_example.in", "b_should_be_easy.in", "c_no_hurry.in", "d_metropolis.in", "e_high_bonus.in"]

# som = 0

# i = Instance(files[3])
# i.printMe()

# s = Solution(i)
# s.init_rides_by_t()
# s.create_chaine(get_max)

# som += s.score()

# print "score ",som



def t():

	files = ["a_example.in", "b_should_be_easy.in", "c_no_hurry.in", "d_metropolis.in", "e_high_bonus.in"]

	functions = [get_max, get_min, get_random]
	som = 0
	for file in files : 


		i = Instance(file)
		#i.printMe()

		tmp_score = 0
		for function in functions:			
			print function
			s = Solution(i)
			s.init_rides_by_t()
			s.create_chaine(function)
			tmp = s.score()
			if tmp > tmp_score:
				tmp_score = tmp
				s.printFile()

		som += tmp_score


	print "score ",som


def t2():

	files = ["a_example.in", "b_should_be_easy.in", "c_no_hurry.in", "d_metropolis.in", "e_high_bonus.in"]

	functions = [min_dist]
	som = 0
	for file in files : 


		i = Instance(file)
		#i.printMe()

		tmp_score = 0
		for function in functions:			
			print function
			s = Solution(i)
			s.init_rides_by_t()
			s.create_chaine_v2(function)
			tmp = s.score()
			if tmp > tmp_score:
				tmp_score = tmp
				s.printFile()

		som += tmp_score


	print "score ",som

#t2()
t()
