# -*- coding: utf-8 -*-
"""
R,C,F,N,B,T
a,b,x,y,s,f
"""

class Instance():

	def __init__(self,file):
		self.file = file
		with open(file,'r') as f:
			l = f.readline()
			self.R,self.C,self.F,self.N,self.B,self.T = [int(a) for a in l.split()]
			self.rides = []
			for l in f.readlines():
				ride = [int(a) for a in l.split()]
				ride.append(ride[-1]-abs(ride[2]-ride[0]) + abs(ride[3] - ride[1])-1) #last star feasible
				self.rides.append(ride)

	def printMe(self):
		print " R = %d \n C = %d \n F = %d \n N = %d \n B = %d \n T = %d\n"%(self.R,self.C,self.F,self.N,self.B,self.T)+"="*25
		print "RIDES"
		print '='*25
		for a in self.rides:
			print a



