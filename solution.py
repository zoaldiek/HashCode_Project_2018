# -*- coding: utf-8 -*-
"""

"""

import random

class Solution():

	def __init__(self,inst):
		self.inst = inst

		self.rides_by_t = []
		self.rides_by_car = []

		self.allocation = [ [] for _ in range(self.inst.F)]

	def init_rides_by_t(self):
		for _ in range(self.inst.T):
			self.rides_by_t.append([])

		# key = earliest time
		for index_ride,ride in enumerate(self.inst.rides):
			self.rides_by_t[ride[4]].append((ride,index_ride))

		for i in range(self.inst.T):
			sorted(self.rides_by_t[i], key = lambda x: self.get_dist(x[0]), reverse = True)


	def get_dist(self, ride):
		return abs(ride[2]-ride[0]) + abs(ride[3] - ride[1])

	def dist(self, pos1,pos2):
		return abs(pos2[1]-pos1[1]) + abs(pos2[0]-pos1[0])

	def gloutonClass(self):
		ridesbylastfeasibleandStartTime = sorted(list(enumerate(self.inst.rides)),key=lambda x:(x[1][-3],-x[1][-1]))
		self.rides_by_car=[[] for _ in xrange(self.inst.F)]
		t = [0 for _ in xrange(self.inst.F)]
		posx = [0 for _ in xrange(self.inst.F)]
		posy = [0 for _ in xrange(self.inst.F)]
		for car in xrange(self.inst.F):
			if t[car]<self.inst.T and ridesbylastfeasibleandStartTime:
				r = min(ridesbylastfeasibleandStartTime,key= lambda x:(x[1][-3]-max((t[car]+self.dist((posx[car],posy[car]),(x[1][0],x[1][1])),x[1][-3]) ) ))
				print r
				t[car]+= self.dist((posx[car],posy[car]),(r[1][0],r[1][1])) + self.get_dist(r[1])
				posx[car] = r[1][2]
				posy[car] = r[1][3]
				ridesbylastfeasibleandStartTime.remove(r)
				self.rides_by_car[car].append(r[0])


	def score(self):
		sc = 0
		for car in self.allocation:
			t = 0
			posx = 0
			posy = 0
			for i in car:
				sr = 0
				a,b,x,y,s,f,mins = self.inst.rides[i]
				#a,b,x,y,s,f = self.inst.rides[i]
				t = max(self.dist((posx,posy),(a,b)),s)
				if t == s:
					sr += self.inst.B
				dis = self.get_dist(self.inst.rides[i])
				if t+dis<f:
					sc+=dis+sr
				posx = x
				posy = y
				t+=dis
		return sc



	# def get_dist(self, a, b, x,y):
	# 	return abs(x-a) + abs(b - y)


	def allocate_by_dist(self):

		## disponibilités a un temps t
		#
		availables = [ [] for _ in range(self.inst.T)]
		availables[0] = [ i for i in range(self.inst.F)]


		current_rides = [ 0 for _ in range(self.inst.F)] 

		for t in range(self.inst.T):

			j = 0

			for ride in self.rides_by_t[t]:
				if j < len(availables[t]):
					index_vehicle = availables[t][j]

					dist = self.get_dist(ride[0])

					availables[t+dist].append(index_vehicle)

					self.allocation[index_vehicle].append(ride[1])
		
					j+=1

		for i in range(len(self.allocation)):
			sorted(self.allocation)


	def create_chaine(self, function_choose):

		ride_dispo = set()
		for i in range (self.inst.N):
			ride_dispo.add(i)

		index_vehicle = 0

		while ride_dispo and index_vehicle < self.inst.F:

			t_tmp = 0

			continu = True

			chaine = []


			while continu:

				# recherche des ride dispos pour un temps donné
				while t_tmp < self.inst.T and not self.rides_by_t[t_tmp]:
					t_tmp += 1

				if t_tmp < self.inst.T:

					(ride, index_ride) = function_choose(self.rides_by_t[t_tmp])
					
					# récupération du ride ayant la plus grane distance 
					#(ride, index_ride) = max(self.rides_by_t[t_tmp], key = lambda x : self.get_dist(x[0]))

					self.rides_by_t[t_tmp].remove((ride,index_ride))
					ride_dispo.remove(index_ride)

					# on cherche maintenant des 
					dist = self.get_dist(ride)

					t_tmp = ride[4] + dist#ride[5]

					chaine.append(index_ride)

					#print chaine

				else:
					continu = False
			###
			self.allocation[index_vehicle] = chaine
			index_vehicle = index_vehicle + 1 


	def printFile(self):
		with open(self.inst.file[:-3]+"_"+str(self.score())+'.out','w') as f:
			for ides in self.allocation:
				f.write("%d %s\n"%(len(ides),' '.join([str(a) for a in ides])))

	def create_chaine_v2(self, function_choose):

		ride_dispo = set()
		for i in range (self.inst.N):
			ride_dispo.add(i)

		index_vehicle = 0

		while ride_dispo and index_vehicle < self.inst.F:

			t_tmp = 0

			continu = True

			chaine = []

			last_coord = (0,0)

			while continu:

				# recherche des ride dispos pour un temps donné
				while t_tmp < self.inst.T and not self.rides_by_t[t_tmp]:
					t_tmp += 1

				if t_tmp < self.inst.T:

					(ride, index_ride) = function_choose(self.rides_by_t[t_tmp], last_coord)
					
					# récupération du ride ayant la plus grane distance 
					#(ride, index_ride) = max(self.rides_by_t[t_tmp], key = lambda x : self.get_dist(x[0]))

					self.rides_by_t[t_tmp].remove((ride,index_ride))
					ride_dispo.remove(index_ride)

					# on cherche maintenant des 

					dist = self.get_dist(ride)

					t_tmp = ride[4] + dist#ride[5]

					last_coord = (ride[2], ride[3])

					chaine.append(index_ride)

					#print chaine

				else:
					continu = False
			###
			self.allocation[index_vehicle] = chaine
			index_vehicle = index_vehicle + 1 










def get_dist_ride(ride):
		return abs(ride[2]-ride[0]) + abs(ride[3] - ride[1])


def get_max(l):
	return max(l, key = lambda x : get_dist_ride(x[0]))
			


def get_min( l):
	return min(l, key = lambda x : get_dist_ride(x[0]))
			

def get_random( l):
	a = random.randint(0 , len(l)-1)
	return l[a]
			



#######################


def get_dist_between_two_points(a , b):
		return abs(a[0]-b[0]) + abs(a[1] - b[1])


def min_dist(l, last_coord):
	min_dist = -1
	sol = (0,0)
	for (ride, index_ride) in l:
		tmp_dist = get_dist_between_two_points(last_coord, (ride[0],ride[1]))
		if tmp_dist < min_dist or min_dist == -1:
			sol = (ride,index_ride)
	return sol

